<div align="center">

## **Welcome to the Soundflow documentation repo.**

![Semantic description of image](https://i.imgur.com/DRlbx7R.png "Soundflow logo")

This repo contains all of the documentation regarding the application. The project consists out of two repo's hosted on Gitlab.com inside the '**soundflow**' group:

**Repo's are hosted on Gitlab.com and mirrored to the FHICT gitlab on git.fhict.nl**
* **Backend**: soundflow-webservice-vertx-openapi https://gitlab.com/soundflow1/soundshare-webservice-vertx-openapi
* **Frontend**: soundflow-webapp https://gitlab.com/soundflow1/soundshare-webapp

**Project management is done via the integrated tools on Gitlab.com**
* Milestones of the project can be found: https://gitlab.com/groups/soundflow1/-/milestones
* Board can be found: https://gitlab.com/groups/soundflow1/-/boards
* Full list of issues can be found: https://gitlab.com/groups/soundflow1/-/issues

**Currently, this repo contains the following files:**
* Software Architecture Document (SAD)
* Research files
* Sprint 0 retrospective

</div>
